/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.ClienteDao;
import model.ContratoDao;
import model.ProdutoDao;

/**
 *
 * @author edwin
 */
@WebServlet(name = "Dashboard", urlPatterns = {"/Dashboard"})
public class Dashboard extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter(); 
       
        String mensagem;
            int rContrato,rCliente,rProduto;
            
            ContratoDao daoContratos = new ContratoDao();
            rContrato = daoContratos.conectar();
            int contratos = daoContratos.consultarTotalContrato();
            request.setAttribute("contratos", contratos);
            daoContratos.desconectar();
            
            ClienteDao daoClientes = new ClienteDao();
             rCliente = daoClientes.conectar();
            int clientes = daoClientes.consultarTotalCliente();
            request.setAttribute("clientes", clientes);
            daoClientes.desconectar();
            
            ProdutoDao daoProdutos = new ProdutoDao();
             rProduto = daoProdutos.conectar();
            int produtos = daoProdutos.consultarTotalProduto();
             request.setAttribute("produtos", produtos);
            daoProdutos.desconectar();
            
            
            rContrato = daoContratos.conectar();
            float contrato6 = daoContratos.consultarMes6Contrato();
            request.setAttribute("contrato6", contrato6);
                       
            float contrato5 = daoContratos.consultarMesIContrato(1);
            request.setAttribute("contrato5", contrato5);
            
            float contrato4 = daoContratos.consultarMesIContrato(2);
            request.setAttribute("contrato4", contrato4);
            
            float contrato3 = daoContratos.consultarMesIContrato(3);
            request.setAttribute("contrato3", contrato3);
            
            float contrato2 = daoContratos.consultarMesIContrato(4);
            request.setAttribute("contrato2", contrato2);
            
            float contrato1 = daoContratos.consultarMesIContrato(5);
            request.setAttribute("contrato1", contrato1);
            
            daoContratos.desconectar();
            
            
                    //mensagem= "Dashboard!!!!";
                    //request.setAttribute("mensagem", mensagem);
                    
            Calendar now = Calendar.getInstance();    
            
           int mes_count = now.get(Calendar.MONTH);
           
           mes_count = mes_count+1;
          
            // fetching updated time
            //Date dateBeforeAMonth = calNow.getTime();
            
            String month1 = Integer.toString(mes_count);
            mes_count=mes_count-1;
            if(mes_count==0){mes_count=12;}
            
            String month2 = Integer.toString(mes_count);
            mes_count=mes_count-1;
            if(mes_count==0){mes_count=12;}
            
            String month3 = Integer.toString(mes_count);
            mes_count=mes_count-1;
            if(mes_count==0){mes_count=12;}
            
            String month4 = Integer.toString(mes_count);
            mes_count=mes_count-1;
            if(mes_count==0){mes_count=12;}
            
            String month5 = Integer.toString(mes_count);
            mes_count=mes_count-1;
            if(mes_count==0){mes_count=12;}
            
            String month6 = Integer.toString(mes_count);
            mes_count=mes_count-1;
            if(mes_count==0){mes_count=12;}
            
//String monthStr = ((month < 10) ? "0" : "") + month;
            //out.print(monthStr);
            String mes6,mes5,mes4,mes3,mes2,mes1;
            mes6 = imprimeMes(month1);
            request.setAttribute("mes6", mes6);
            mes5 = imprimeMes(month2);
            request.setAttribute("mes5", mes5);
            mes4 = imprimeMes(month3);
            request.setAttribute("mes4", mes4);
            mes3 = imprimeMes(month4);
            request.setAttribute("mes3", mes3);
            mes2 = imprimeMes(month5);
            request.setAttribute("mes2", mes2);
            mes1 = imprimeMes(month6);
            request.setAttribute("mes1", mes1);
            
                    RequestDispatcher d = request.getRequestDispatcher("index.jsp");
                    d.forward(request, response);  
            
        
    }
    public String imprimeMes(String mes){
        String mes_imprime;
        switch(mes){
                       case "1":
                            mes_imprime = "Janeiro";
                            break;
                        case "2":
                            mes_imprime = "Fevereiro";
                            break;
                        case "3":
                            mes_imprime = "Março";
                            break;
                        case "4":
                            mes_imprime = "Abril";
                            break;
                        case "5":
                            mes_imprime = "Maio";
                            break;
                        case "6":
                            mes_imprime = "Junho";
                            break;
                        case "7":
                            mes_imprime = "Julho";
                            break;    
                        case "8":
                            mes_imprime = "Agosto";
                            break;
                        case "9":
                            mes_imprime = "Setembro";
                            break;
                        case "10":
                            mes_imprime = "Outubro";
                            break;
                        case "11":
                            mes_imprime = "Novembro";
                            break;
                        case "12":
                            mes_imprime = "Dezembro";
                            break;
                        default:
                            mes_imprime = mes;
                            break;
                    }        
            
        return mes_imprime;
    
    }
    
 @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Dashboard.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
