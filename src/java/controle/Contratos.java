/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.ClienteDao;
import model.Contrato;
import model.ContratoDao;

/**
 *
 * @author edwin
 */
@WebServlet(name = "Contratos", urlPatterns = {"/Contratos"})
public class Contratos extends HttpServlet{
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter(); 
        String flag = request.getParameter("flag");
        int id;
        String mensagem,proposta,cliente,data_proposta,tipo_contrato,valor,
                forma_pagamento,indice_reajuste,mes_reajuste,prazo,validade,
                inicio,conclusao,termino,status,titulo,detalhes;
        
        if (flag.equalsIgnoreCase("cadastracontrato")) {
            
            proposta = request.getParameter("proposta");
            cliente = request.getParameter("cliente");
            data_proposta = request.getParameter("data_proposta");
            tipo_contrato = request.getParameter("tipo_contrato");
            valor = request.getParameter("valor");
            forma_pagamento = request.getParameter("forma_pagamento");
            indice_reajuste = request.getParameter("indice_reajuste");
            mes_reajuste = request.getParameter("mes_reajuste");
            prazo = request.getParameter("prazo");
            validade = request.getParameter("validade");
            inicio = request.getParameter("inicio");
            conclusao = request.getParameter("conclusao");
            termino = request.getParameter("termino");
            status = request.getParameter("status");
            titulo = request.getParameter("titulo");
            detalhes = request.getParameter("detalhes");
            
            
            Contrato contrato = new Contrato();  
//criou um objeto ou variavel chamado cli//
//Cliente: nome da classe | cli=new:nome do objeto |Cliente():nome do construtor//
//.set metodo para incluir os valores do objeto//
            contrato.setProposta(proposta);    
            contrato.setCliente(cliente);
            contrato.setData_proposta(data_proposta);
            contrato.setTipo_contrato(tipo_contrato);
            contrato.setValor(valor);
            contrato.setForma_pagamento(forma_pagamento);
            contrato.setIndice_reajuste(indice_reajuste);
            contrato.setMes_reajuste(mes_reajuste);
            contrato.setPrazo(prazo);
            contrato.setValidade(validade);
            contrato.setInicio(inicio);
            contrato.setConclusao(conclusao);
            contrato.setTermino(termino);
            contrato.setStatus(status);
            contrato.setTitulo(titulo);
            contrato.setDetalhes(detalhes);
            
            ContratoDao dao = new ContratoDao();
            int r = dao.conectar();
// o método conectar só retorna 0 ou 1, portando r recebe a resposta do metodo//
            if (r == 0) {
                mensagem = "Erro ao se conectar ao banco de dados";
                request.setAttribute("mensagem", mensagem);
                RequestDispatcher d = request.getRequestDispatcher("erro.jsp");
                d.forward(request, response);
            } else {
                r = dao.cadastraContrato(contrato);
                if (r == 1) {
                    //mensagem = "Cliente Cadastrado!!!";
                    //request.setAttribute("mensagem", mensagem);
                    //RequestDispatcher d = request.getRequestDispatcher("cadastrosucesso.jsp");
                    //d.forward(request, response);
                    ArrayList lista = dao.consultarTodosContratos();
                if (!lista.isEmpty()) {
                    request.setAttribute("lista", lista);
                    RequestDispatcher d = request.getRequestDispatcher("exibir_contrato.jsp");
                    d.forward(request, response);
                    return;
                } else {
                    mensagem = "Contrato não encontrado";
                    request.setAttribute("mensagem", mensagem);
                    RequestDispatcher d = request.getRequestDispatcher("mensagens.jsp");
                    d.forward(request, response);
                }
                } else {
                    mensagem = "Ocorreu algum erro!!!";
                    request.setAttribute("mensagem", mensagem);
                    RequestDispatcher d = request.getRequestDispatcher("erro.jsp");
                    d.forward(request, response);
                }
                dao.desconectar();
            }
            request.setAttribute("mensagem", mensagem);
            RequestDispatcher d = request.getRequestDispatcher("cadastrosucesso.jsp");
            d.forward(request, response); 
        
        }else if(flag.equalsIgnoreCase("consultatodoscontratos")){
           
            ContratoDao dao = new ContratoDao();
            int r = dao.conectar();//recebe 0 ou 1 para saber se conectou ou não
            if (r == 0) {
                mensagem = "erro na conexão com o bd";
                request.setAttribute("mensagem", mensagem);
                RequestDispatcher d = request.getRequestDispatcher("erro.jsp");
                d.forward(request, response);                
            } else { //conectou ao banco de dados
                ArrayList lista = dao.consultarTodosContratos();
                if (!lista.isEmpty()) {
                    request.setAttribute("lista", lista);
                    RequestDispatcher d = request.getRequestDispatcher("exibir_contrato.jsp");
                    d.forward(request, response);
                    return;
                } else {
                    mensagem = "Contrato não encontrado";
                    request.setAttribute("mensagem", mensagem);
                    RequestDispatcher d = request.getRequestDispatcher("mensagens.jsp");
                    d.forward(request, response);
                }
                dao.desconectar();
            } 
        
        
        } else if (flag.equalsIgnoreCase("alteraContrato")) {
        
        id = Integer.parseInt(request.getParameter("id"));
        ContratoDao dao = new ContratoDao();
        dao.conectar();
        ArrayList lista = dao.consultarContratoId(id);
                if (!lista.isEmpty()) {
                    request.setAttribute("lista", lista);
                    RequestDispatcher d = request.getRequestDispatcher("editar_contrato.jsp");
                    d.forward(request, response);
                    return;
                } else {
                    mensagem = "Contrato não encontrado";
                    request.setAttribute("mensagem", mensagem);
                    RequestDispatcher d = request.getRequestDispatcher("mensagens.jsp");
                    d.forward(request, response);
                }
                dao.desconectar();
        
       //  ClienteDao dao = new ClienteDao();
       //  request.setAttribute("cliente", cliente);
            
    
    }else if (flag.equalsIgnoreCase("salvaContrato")) {
            
            id  = Integer.parseInt(request.getParameter("id"));
            proposta = request.getParameter("proposta");
            cliente = request.getParameter("cliente");
            data_proposta = request.getParameter("data_proposta");
            tipo_contrato = request.getParameter("tipo_contrato");
            valor = request.getParameter("valor");
            forma_pagamento = request.getParameter("forma_pagamento");
            indice_reajuste = request.getParameter("indice_reajuste");
            mes_reajuste = request.getParameter("mes_reajuste");
            prazo = request.getParameter("prazo");
            validade = request.getParameter("validade");
            inicio = request.getParameter("inicio");
            conclusao = request.getParameter("conclusao");
            termino = request.getParameter("termino");
            status = request.getParameter("status");
            titulo = request.getParameter("titulo");
            detalhes = request.getParameter("detalhes");
            
            Contrato contrato = new Contrato();  

            contrato.setId(id);
            contrato.setProposta(proposta);    
            contrato.setCliente(cliente);
            contrato.setData_proposta(data_proposta);
            contrato.setTipo_contrato(tipo_contrato);
            contrato.setValor(valor);
            contrato.setForma_pagamento(forma_pagamento);
            contrato.setIndice_reajuste(indice_reajuste);
            contrato.setMes_reajuste(mes_reajuste);
            contrato.setPrazo(prazo);
            contrato.setValidade(validade);
            contrato.setInicio(inicio);
            contrato.setConclusao(conclusao);
            contrato.setTermino(termino);
            contrato.setStatus(status);
            contrato.setTitulo(titulo);
            contrato.setDetalhes(detalhes);
            
            ContratoDao dao = new ContratoDao();
            int r = dao.conectar();
// o método conectar só retorna 0 ou 1, portando r recebe a resposta do metodo//
            if (r == 0) {
                mensagem = "Erro ao se conectar ao banco de dados";
                request.setAttribute("mensagem", mensagem);
                RequestDispatcher d = request.getRequestDispatcher("erro.jsp");
                d.forward(request, response);
            } else {
                r = dao.alteraContrato(contrato);
                if (r == 1) {
                    //mensagem = "Cliente Cadastrado!!!";
                    //request.setAttribute("mensagem", mensagem);
                    //RequestDispatcher d = request.getRequestDispatcher("cadastrosucesso.jsp");
                    //d.forward(request, response);
                    ArrayList lista = dao.consultarTodosContratos();
                if (!lista.isEmpty()) {
                    request.setAttribute("lista", lista);
                    RequestDispatcher d = request.getRequestDispatcher("exibir_contrato.jsp");
                    d.forward(request, response);
                    return;
                } else {
                    mensagem = "Contrato não encontrado";
                    request.setAttribute("mensagem", mensagem);
                    RequestDispatcher d = request.getRequestDispatcher("mensagens.jsp");
                    d.forward(request, response);
                }
                } else {
                    mensagem = "Ocorreu algum erro!!!";
                    request.setAttribute("mensagem", mensagem);
                    RequestDispatcher d = request.getRequestDispatcher("erro.jsp");
                    d.forward(request, response);
                }
                dao.desconectar();
            }
            request.setAttribute("mensagem", mensagem);
            RequestDispatcher d = request.getRequestDispatcher("cadastrosucesso.jsp");
            d.forward(request, response); 
            
    }else if (flag.equalsIgnoreCase("deletaContrato")) {
        id = Integer.parseInt(request.getParameter("id"));
        ContratoDao dao = new ContratoDao();
        dao.conectar();
        dao.deletarContrato(id);
        ArrayList lista = dao.consultarTodosContratos();
                if (!lista.isEmpty()) {
                    request.setAttribute("lista", lista);
                    RequestDispatcher d = request.getRequestDispatcher("exibir_contrato.jsp");
                    d.forward(request, response);
                    return;
                } else {
                    mensagem = "Contrato não encontrado";
                    request.setAttribute("mensagem", mensagem);
                    RequestDispatcher d = request.getRequestDispatcher("mensagens.jsp");
                    d.forward(request, response);
                }
                    
        dao.desconectar();
        
        
    }else{
            
            mensagem= "Erro na flag!!!!";
            request.setAttribute("mensagem", mensagem);
            RequestDispatcher d = request.getRequestDispatcher("erro.jsp");
            d.forward(request, response);   
        
        
        }
        
    }
    
    
    
    
@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

   
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
}
