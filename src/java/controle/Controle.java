package controle;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Produto;
import model.Cliente;

import model.ClienteDao;
import model.ProdutoDao;


@WebServlet(name = "Controle", urlPatterns = {"/Controle"})
public class Controle extends HttpServlet {

   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String mensagem;
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter(); 
        String nome,tel,email;
        int idade,id;
        float preco;
        String flag = request.getParameter("flag");
        
        if (flag.equalsIgnoreCase("cadastracli")) {
       
            nome = request.getParameter("nome");
           
            tel = request.getParameter("tel");
            idade = Integer.parseInt(request.getParameter("idade"));
            email = request.getParameter("email");
            
            Cliente cli = new Cliente();  
//criou um objeto ou variavel chamado cli//
//Cliente: nome da classe | cli=new:nome do objeto |Cliente():nome do construtor//
//.set metodo para incluir os valores do objeto//
            cli.setIdade(idade);    
            cli.setNome(nome);
            cli.setTel(tel);
            cli.setEmail(email);
            
            ClienteDao dao = new ClienteDao();
            int r = dao.conectar();
// o método conectar só retorna 0 ou 1, portando r recebe a resposta do metodo//
            if (r == 0) {
                mensagem = "Erro ao se conectar ao banco de dados";
                request.setAttribute("mensagem", mensagem);
                RequestDispatcher d = request.getRequestDispatcher("erro.jsp");
                d.forward(request, response);
            } else {
                r = dao.cadastraCliente(cli);
                if (r == 1) {
                    //mensagem = "Cliente Cadastrado!!!";
                    //request.setAttribute("mensagem", mensagem);
                    //RequestDispatcher d = request.getRequestDispatcher("cadastrosucesso.jsp");
                    //d.forward(request, response);
                    ArrayList lista = dao.consultarTodosCliente();
                if (!lista.isEmpty()) {
                    request.setAttribute("lista", lista);
                    RequestDispatcher d = request.getRequestDispatcher("exibir_cliente.jsp");
                    d.forward(request, response);
                    return;
                } else {
                    mensagem = "Usuario não encontrado";
                    request.setAttribute("mensagem", mensagem);
                    RequestDispatcher d = request.getRequestDispatcher("mensagens.jsp");
                    d.forward(request, response);
                }
                } else {
                    mensagem = "Ocorreu algum erro!!!";
                    request.setAttribute("mensagem", mensagem);
                    RequestDispatcher d = request.getRequestDispatcher("erro.jsp");
                    d.forward(request, response);
                }
                dao.desconectar();
            }
            request.setAttribute("mensagem", mensagem);
            RequestDispatcher d = request.getRequestDispatcher("cadastrosucesso.jsp");
            d.forward(request, response); 
            
    }else if (flag.equalsIgnoreCase("cadastrapro")) {
            id = Integer.parseInt(request.getParameter("codigo"));
            nome = request.getParameter("nome");
            preco = Float.parseFloat(request.getParameter("preco"));
          
            Produto pro = new Produto();  
//criou um objeto ou variavel chamado cli//
//Cliente: nome da classe | cli=new:nome do objeto |Cliente():nome do construtor//
//.set metodo para incluir os valores do objeto//
            pro.setCodigo(id);    
            pro.setNome(nome);
            pro.setPreco(preco);
           
            
            ProdutoDao dao = new ProdutoDao();
            int r = dao.conectar();
// o método conectar só retorna 0 ou 1, portando r recebe a resposta do metodo//
            if (r == 0) {
                mensagem = "Erro ao se conectar ao banco de dados";
                request.setAttribute("mensagem", mensagem);
                RequestDispatcher d = request.getRequestDispatcher("erro.jsp");
                d.forward(request, response);
            } else {
                r = dao.cadastraProduto(pro);
                if (r == 1) {
                    //mensagem = "Cliente Cadastrado!!!";
                    //request.setAttribute("mensagem", mensagem);
                    //RequestDispatcher d = request.getRequestDispatcher("cadastrosucesso.jsp");
                    //d.forward(request, response);
                    ArrayList lista = dao.consultarTodosProduto();
                if (!lista.isEmpty()) {
                    request.setAttribute("lista", lista);
                    RequestDispatcher d = request.getRequestDispatcher("exibir_produto.jsp");
                    d.forward(request, response);
                    return;
                } else {
                    mensagem = "Produto não encontrado";
                    request.setAttribute("mensagem", mensagem);
                    RequestDispatcher d = request.getRequestDispatcher("mensagens.jsp");
                    d.forward(request, response);
                }
                } else {
                    mensagem = "Ocorreu algum erro!!!";
                    request.setAttribute("mensagem", mensagem);
                    RequestDispatcher d = request.getRequestDispatcher("erro.jsp");
                    d.forward(request, response);
                }
                dao.desconectar();
            }
            request.setAttribute("mensagem", mensagem);
            RequestDispatcher d = request.getRequestDispatcher("cadastrosucesso.jsp");
            d.forward(request, response); 
            
    }else if (flag.equalsIgnoreCase("salvacli")) {
            id  = Integer.parseInt(request.getParameter("id"));
            nome = request.getParameter("nome");
            tel = request.getParameter("tel");
            idade = Integer.parseInt(request.getParameter("idade"));
            email = request.getParameter("email");
            
            Cliente cli = new Cliente();  
//criou um objeto ou variavel chamado cli//
//Cliente: nome da classe | cli=new:nome do objeto |Cliente():nome do construtor//
//.set metodo para incluir os valores do objeto//
            cli.setId(id); 
            cli.setIdade(idade);    
            cli.setNome(nome);
            cli.setTel(tel);
            cli.setEmail(email);
            
            ClienteDao dao = new ClienteDao();
            int r = dao.conectar();
// o método conectar só retorna 0 ou 1, portando r recebe a resposta do metodo//
            if (r == 0) {
                mensagem = "Erro ao se conectar ao banco de dados";
                request.setAttribute("mensagem", mensagem);
                RequestDispatcher d = request.getRequestDispatcher("erro.jsp");
                d.forward(request, response);
            } else {
                r = dao.alteraCliente(cli);
                if (r == 1) {
                    //mensagem = "Cliente Cadastrado!!!";
                    //request.setAttribute("mensagem", mensagem);
                    //RequestDispatcher d = request.getRequestDispatcher("cadastrosucesso.jsp");
                    //d.forward(request, response);
                    ArrayList lista = dao.consultarTodosCliente();
                if (!lista.isEmpty()) {
                    request.setAttribute("lista", lista);
                    RequestDispatcher d = request.getRequestDispatcher("exibir_cliente.jsp");
                    d.forward(request, response);
                    return;
                } else {
                    mensagem = "Usuario não encontrado";
                    request.setAttribute("mensagem", mensagem);
                    RequestDispatcher d = request.getRequestDispatcher("mensagens.jsp");
                    d.forward(request, response);
                }
                } else {
                    mensagem = "Ocorreu algum erro!!!";
                    request.setAttribute("mensagem", mensagem);
                    RequestDispatcher d = request.getRequestDispatcher("erro.jsp");
                    d.forward(request, response);
                }
                dao.desconectar();
            }
            request.setAttribute("mensagem", mensagem);
            RequestDispatcher d = request.getRequestDispatcher("cadastrosucesso.jsp");
            d.forward(request, response); 
            
    } else if (flag.equalsIgnoreCase("salvaproduto")) {
        
            id = Integer.parseInt(request.getParameter("codigo"));
            nome = request.getParameter("nome");
            preco = Float.parseFloat(request.getParameter("preco"));
            
            
            Produto pro = new Produto();  
//criou um objeto ou variavel chamado cli//
//Cliente: nome da classe | cli=new:nome do objeto |Cliente():nome do construtor//
//.set metodo para incluir os valores do objeto//
            pro.setCodigo(id);     
            pro.setNome(nome);
            pro.setPreco(preco);
          
            
            ProdutoDao dao = new ProdutoDao();
            int r = dao.conectar();
// o método conectar só retorna 0 ou 1, portando r recebe a resposta do metodo//
            if (r == 0) {
                mensagem = "Erro ao se conectar ao banco de dados";
                request.setAttribute("mensagem", mensagem);
                RequestDispatcher d = request.getRequestDispatcher("erro.jsp");
                d.forward(request, response);
            } else {
                r = dao.alteraProduto(pro);
                if (r == 1) {
                    //mensagem = "Cliente Cadastrado!!!";
                    //request.setAttribute("mensagem", mensagem);
                    //RequestDispatcher d = request.getRequestDispatcher("cadastrosucesso.jsp");
                    //d.forward(request, response);
                    ArrayList lista = dao.consultarTodosProduto();
                if (!lista.isEmpty()) {
                    request.setAttribute("lista", lista);
                    RequestDispatcher d = request.getRequestDispatcher("exibir_produto.jsp");
                    d.forward(request, response);
                    return;
                } else {
                    mensagem = "Usuario não encontrado";
                    request.setAttribute("mensagem", mensagem);
                    RequestDispatcher d = request.getRequestDispatcher("mensagens.jsp");
                    d.forward(request, response);
                }
                } else {
                    mensagem = "Ocorreu algum erro!!!";
                    request.setAttribute("mensagem", mensagem);
                    RequestDispatcher d = request.getRequestDispatcher("erro.jsp");
                    d.forward(request, response);
                }
                dao.desconectar();
            }
            request.setAttribute("mensagem", mensagem);
            RequestDispatcher d = request.getRequestDispatcher("cadastrosucesso.jsp");
            d.forward(request, response); 
            
            
    } else if (flag.equalsIgnoreCase("alteraCliente")) {
        
        id = Integer.parseInt(request.getParameter("id"));
        ClienteDao dao = new ClienteDao();
        dao.conectar();
        ArrayList lista = dao.consultarClienteId(id);
                if (!lista.isEmpty()) {
                    request.setAttribute("lista", lista);
                    RequestDispatcher d = request.getRequestDispatcher("editar_cliente.jsp");
                    d.forward(request, response);
                    return;
                } else {
                    mensagem = "Usuario não encontrado";
                    request.setAttribute("mensagem", mensagem);
                    RequestDispatcher d = request.getRequestDispatcher("mensagens.jsp");
                    d.forward(request, response);
                }
                dao.desconectar();
        
       //  ClienteDao dao = new ClienteDao();
       //  request.setAttribute("cliente", cliente);
            
    
    }else if (flag.equalsIgnoreCase("alteraProduto")) {
        
        id = Integer.parseInt(request.getParameter("id"));
        ProdutoDao dao = new ProdutoDao();
        dao.conectar();
        ArrayList lista = dao.consultarProdutoId(id);
                if (!lista.isEmpty()) {
                    request.setAttribute("lista", lista);
                    RequestDispatcher d = request.getRequestDispatcher("editar_produto.jsp");
                    d.forward(request, response);
                    return;
                } else {
                    mensagem = "Produto não encontrado";
                    request.setAttribute("mensagem", mensagem);
                    RequestDispatcher d = request.getRequestDispatcher("mensagens.jsp");
                    d.forward(request, response);
                }
                dao.desconectar();
        
       //  ClienteDao dao = new ClienteDao();
       //  request.setAttribute("cliente", cliente);
            
    
    } else if (flag.equalsIgnoreCase("deletaProduto")) {
        id = Integer.parseInt(request.getParameter("id"));
        ProdutoDao dao = new ProdutoDao();
        dao.conectar();
        dao.deletarProduto(id);
        ArrayList lista = dao.consultarTodosProduto();
                if (!lista.isEmpty()) {
                    request.setAttribute("lista", lista);
                    RequestDispatcher d = request.getRequestDispatcher("exibir_produto.jsp");
                    d.forward(request, response);
                    return;
                } else {
                    mensagem = "Produto não encontrado";
                    request.setAttribute("mensagem", mensagem);
                    RequestDispatcher d = request.getRequestDispatcher("mensagens.jsp");
                    d.forward(request, response);
                }
                    
        dao.desconectar();
        
        
    } else if (flag.equalsIgnoreCase("deletaCliente")) {
        id = Integer.parseInt(request.getParameter("id"));
        ClienteDao dao = new ClienteDao();
        dao.conectar();
        dao.deletarCliente(id);
        ArrayList lista = dao.consultarTodosCliente();
                if (!lista.isEmpty()) {
                    request.setAttribute("lista", lista);
                    RequestDispatcher d = request.getRequestDispatcher("exibir_cliente.jsp");
                    d.forward(request, response);
                    return;
                } else {
                    mensagem = "Usuario não encontrado";
                    request.setAttribute("mensagem", mensagem);
                    RequestDispatcher d = request.getRequestDispatcher("mensagens.jsp");
                    d.forward(request, response);
                }
                    
        dao.desconectar();
        
        
    }
    else if (flag.equalsIgnoreCase("consultarclientenome")) {
            nome = request.getParameter("nome");

            ClienteDao dao = new ClienteDao();
            int r = dao.conectar();//recebe 0 ou 1 para saber se conectou ou não
            if (r == 0) {
                mensagem = "erro na conexão com o bd";
                request.setAttribute("mensagem", mensagem);
                RequestDispatcher d = request.getRequestDispatcher("erro.jsp");
                d.forward(request, response);                
            } else { //conectou ao banco de dados
                ArrayList lista = dao.consultarClienteNome(nome);
                if (!lista.isEmpty()) {
                    request.setAttribute("lista", lista);
                    RequestDispatcher d = request.getRequestDispatcher("exibir_cliente.jsp");
                    d.forward(request, response);
                    return;
                } else {
                    mensagem = "Usuario não encontrado";
                    request.setAttribute("mensagem", mensagem);
                    RequestDispatcher d = request.getRequestDispatcher("mensagens.jsp");
                    d.forward(request, response);
                }
                dao.desconectar();
            }
    }
          else if(flag.equalsIgnoreCase("consultartodosclientes")) {
            ClienteDao dao = new ClienteDao();
            int r = dao.conectar();//recebe 0 ou 1 para saber se conectou ou não
            if (r == 0) {
                mensagem = "erro na conexão com o bd";
                request.setAttribute("mensagem", mensagem);
                RequestDispatcher d = request.getRequestDispatcher("erro.jsp");
                d.forward(request, response);                
            } else { //conectou ao banco de dados
                ArrayList lista = dao.consultarTodosCliente();
                if (!lista.isEmpty()) {
                    request.setAttribute("lista", lista);
                    RequestDispatcher d = request.getRequestDispatcher("exibir_cliente.jsp");
                    d.forward(request, response);
                    return;
                } else {
                    mensagem = "Usuario não encontrado";
                    request.setAttribute("mensagem", mensagem);
                    RequestDispatcher d = request.getRequestDispatcher("mensagens.jsp");
                    d.forward(request, response);
                }
                dao.desconectar();
            }   
            
            
        }else if(flag.equalsIgnoreCase("consultartodosproduto")) {
            ProdutoDao dao = new ProdutoDao();
            int r = dao.conectar();//recebe 0 ou 1 para saber se conectou ou não
            if (r == 0) {
                mensagem = "erro na conexão com o bd";
                request.setAttribute("mensagem", mensagem);
                RequestDispatcher d = request.getRequestDispatcher("erro.jsp");
                d.forward(request, response);                
            } else { //conectou ao banco de dados
                ArrayList lista = dao.consultarTodosProduto();
                if (!lista.isEmpty()) {
                    request.setAttribute("lista", lista);
                    RequestDispatcher d = request.getRequestDispatcher("exibir_produto.jsp");
                    d.forward(request, response);
                    return;
                } else {
                    mensagem = "Produto não encontrado";
                    request.setAttribute("mensagem", mensagem);
                    RequestDispatcher d = request.getRequestDispatcher("mensagens.jsp");
                    d.forward(request, response);
                }
                dao.desconectar();
            }   
            
            
        }else{
            mensagem= "Erro na flag!!!!";
            request.setAttribute("mensagem", mensagem);
            RequestDispatcher d = request.getRequestDispatcher("erro.jsp");
            d.forward(request, response);   
        }

    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}


