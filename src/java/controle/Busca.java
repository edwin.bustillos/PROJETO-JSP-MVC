/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.ClienteDao;
import model.ContratoDao;
import model.ProdutoDao;

/**
 *
 * @author edwin
 */
@WebServlet(name = "Busca", urlPatterns = {"/Busca"})
public class Busca extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter(); 
        String flag = request.getParameter("flag");
        String mensagem;
        
        if (flag.equalsIgnoreCase("clientes")) {
            
            String nome;
            nome = request.getParameter("busca");

            ClienteDao dao = new ClienteDao();
            int r = dao.conectar();//recebe 0 ou 1 para saber se conectou ou não
            if (r == 0) {
                mensagem = "erro na conexão com o bd";
                request.setAttribute("mensagem", mensagem);
                RequestDispatcher d = request.getRequestDispatcher("erro.jsp");
                d.forward(request, response);                
            } else { //conectou ao banco de dados
                ArrayList lista = dao.consultarClienteNome(nome);
                if (!lista.isEmpty()) {
                    request.setAttribute("lista", lista);
                    RequestDispatcher d = request.getRequestDispatcher("exibir_cliente.jsp");
                    d.forward(request, response);
                    return;
                } else {
                    mensagem = "Usuario não encontrado";
                    request.setAttribute("mensagem", mensagem);
                    RequestDispatcher d = request.getRequestDispatcher("mensagens.jsp");
                    d.forward(request, response);
                }
                dao.desconectar();
            }
            
        }else if(flag.equalsIgnoreCase("produtos")){
            
            String nome;
            nome = request.getParameter("busca");

            ProdutoDao dao = new ProdutoDao();
            int r = dao.conectar();//recebe 0 ou 1 para saber se conectou ou não
            if (r == 0) {
                mensagem = "erro na conexão com o bd";
                request.setAttribute("mensagem", mensagem);
                RequestDispatcher d = request.getRequestDispatcher("erro.jsp");
                d.forward(request, response);                
            } else { //conectou ao banco de dados
                ArrayList lista = dao.consultarProdutoNome(nome);
                if (!lista.isEmpty()) {
                    request.setAttribute("lista", lista);
                    RequestDispatcher d = request.getRequestDispatcher("exibir_produto.jsp");
                    d.forward(request, response);
                    return;
                } else {
                    mensagem = "Produto não encontrado";
                    request.setAttribute("mensagem", mensagem);
                    RequestDispatcher d = request.getRequestDispatcher("mensagens.jsp");
                    d.forward(request, response);
                }
                dao.desconectar();
            }
        
        }else if(flag.equalsIgnoreCase("contratos")){
        
            String nome;
            nome = request.getParameter("busca");

            ContratoDao dao = new ContratoDao();
            int r = dao.conectar();//recebe 0 ou 1 para saber se conectou ou não
            if (r == 0) {
                mensagem = "erro na conexão com o bd";
                request.setAttribute("mensagem", mensagem);
                RequestDispatcher d = request.getRequestDispatcher("erro.jsp");
                d.forward(request, response);                
            } else { //conectou ao banco de dados
                ArrayList lista = dao.consultarContratoNome(nome);
                if (!lista.isEmpty()) {
                    request.setAttribute("lista", lista);
                    RequestDispatcher d = request.getRequestDispatcher("exibir_contrato.jsp");
                    d.forward(request, response);
                    return;
                } else {
                    mensagem = "Produto não encontrado";
                    request.setAttribute("mensagem", mensagem);
                    RequestDispatcher d = request.getRequestDispatcher("mensagens.jsp");
                    d.forward(request, response);
                }
                dao.desconectar();
            }
            
        }else{
            
            mensagem= "Erro na flag!!!!";
            request.setAttribute("mensagem", mensagem);
            RequestDispatcher d = request.getRequestDispatcher("erro.jsp");
            d.forward(request, response);   
        }
        
    }
    
 @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
}
