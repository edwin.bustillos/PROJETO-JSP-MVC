/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

//aqui utilizar classes para conectar com o banco de dados//
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ClienteDao {
    private Connection conexao;      /*declarando uma variavel  exemplo:Connection
     * (que é uma das classes que faz parte do pacote JDBC - classes que acessam o banco de dados*/

    private Statement st;        // Statement é uma autorização que permite executar comandos SQL//
    // toda vez que for consultar o banco de dados, oque vem do banco de dados, fica armazenado no Objeto (rs) ResultSet//
    
     private ResultSet rs;
    /* todo metodo é declarado sua visibilidade ex:public... tipo de retorno:int e os parametros:(oque vai aqui dentro)*/
    
    public int conectar() {
        try {
            Class.forName("com.mysql.jdbc.Driver");   //classe que vai ser uzada para fazer a ligação com o banco de dados//
            conexao = DriverManager.getConnection("jdbc:mysql://localhost:3306/dados", "root", "senha");
            st=conexao.createStatement();
            return 1;
        } catch (SQLException ex) {
            return 0;
        } catch (ClassNotFoundException x) {
            return 0;
        }
    }
    // cli é um objeto que receberá os atributos contendo os dados do formulário//
    public int cadastraCliente(Cliente cli) {
        try {
            
              String comando = "INSERT INTO cliente (nome,telefone,idade,email)VALUES ('"+cli.getNome()+"','"+cli.getTel()+"','"+cli.getIdade()+"','"+cli.getEmail()+"')" ; 
              st.executeUpdate(comando);
            return 1;
        } catch (SQLException ex) {
            /* quando o programador deseja saber sobre o erro digitar as duas linhas de código abaixo:
            int codErro=ex.getErrorCode();
            String mensagemErro=ex.getMessage();*/
            if (ex.getErrorCode() == 1062) {
                return 2;
            } else {
                return 0;
            }
        }
    }
    public int alteraCliente(Cliente cli) {
        try {
            
              String comando = "UPDATE cliente SET nome='"+cli.getNome()+"',telefone='"+cli.getTel()+"',idade='"+cli.getIdade()+"',email='"+cli.getEmail()+"' WHERE id='"+cli.getId()+"'" ; 
              st.executeUpdate(comando);
            return 1;
        } catch (SQLException ex) {
            /* quando o programador deseja saber sobre o erro digitar as duas linhas de código abaixo:
            int codErro=ex.getErrorCode();
            String mensagemErro=ex.getMessage();*/
             
            if (ex.getErrorCode() == 1062) {
                return 2;
            } else {
                return 0;
            }
        }
    
    }
    
    
       public ArrayList consultarClienteNome(String nome) {
        try {
            String sql = "select * FROM cliente WHERE nome LIKE '%"+nome+"%' OR email LIKE '%"+nome+"%' OR telefone LIKE '%"+nome+"%'";
            ArrayList<Cliente> lista = new ArrayList<>();
            rs=st.executeQuery(sql);
            while (rs.next()){
                Cliente cli = new Cliente();
                cli.setId(rs.getInt("id"));
                cli.setNome(rs.getString("nome"));
                cli.setTel(rs.getString("telefone"));
                cli.setEmail(rs.getString("email"));
                cli.setIdade(rs.getInt("idade"));
                                             
                lista.add(cli);
            }
            return lista;
        } catch (SQLException ex) {
            return null;
        }
    }
    
         public ArrayList consultarClienteId(int id) {
        try {
            String sql = "select * from cliente where id='"+id+"'";
            ArrayList<Cliente> lista = new ArrayList<>();
            rs=st.executeQuery(sql);
            while (rs.next()){
                Cliente cli = new Cliente();
                cli.setId(rs.getInt("id"));
                cli.setNome(rs.getString("nome"));
                cli.setTel(rs.getString("telefone"));
                cli.setEmail(rs.getString("email"));
                cli.setIdade(rs.getInt("idade"));
                                             
                lista.add(cli);
            }
            return lista;
        } catch (SQLException ex) {
            return null;
        }
    }
         
       public ArrayList consultarTodosCliente() {
        try {
            String sql = "select * from cliente";
            ArrayList<Cliente> lista = new ArrayList<>();
            rs=st.executeQuery(sql);
            while (rs.next()){
                Cliente cli = new Cliente();
                cli.setId(rs.getInt("id"));
                cli.setNome(rs.getString("nome"));
                cli.setTel(rs.getString("telefone"));
                cli.setEmail(rs.getString("email"));
                cli.setIdade(rs.getInt("idade"));
                                             
                lista.add(cli);
            }
            return lista;
        } catch (SQLException ex) {
            return null;
        }
    }    
    
    public void deletarCliente(int id){
        
          try {
            
              String comando = "DELETE FROM cliente WHERE id='"+id+"'" ; 
              st.executeUpdate(comando);
           
        } catch (SQLException ex) {
            /* quando o programador deseja saber sobre o erro digitar as duas linhas de código abaixo:
            int codErro=ex.getErrorCode();
            String mensagemErro=ex.getMessage();*/
            
        }
    
    }   
    public int consultarTotalCliente() throws Exception  {
        try {
            String sql = "select * from cliente";
            rs=st.executeQuery(sql);
            int count = 0;
            while (rs.next()){
            count++;    
            }
            return count;
        } catch (Exception e) {
            
        }
        return 0;
    }
    public void desconectar() {
        try {
            conexao.close();
        } catch (SQLException ex) {
           ex.getMessage();
        }
    }
}
