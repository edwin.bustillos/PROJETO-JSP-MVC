/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author edwin
 */
public class ContratoDao {
    private final String user = "root";
    private final String password = "senha";
    private final String url = "jdbc:mysql://localhost:3306/dados";
    private Connection con;
    private Statement st;
    private ResultSet rs;

    public int conectar(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, user, password);
            st = con.createStatement();
            return 1;
        } catch (SQLException e) {
            e.getMessage();
            return 0;
        } catch (ClassNotFoundException e){
            e.getMessage();
            return 0;
        }
    }

    public int desconectar() {
        try {
            st.close();
            con.close();
            return 0;
        } catch (SQLException ex) {
           ex.getMessage();
           return 1;
        }
    }
   
    public List listar() {
        List<Contrato> lista = new ArrayList<>();
        try {
            rs = st.executeQuery("SELECT * FROM contrato");
            while (rs.next()) {
                Contrato contrato=new Contrato();
                contrato.setProposta(rs.getString("proposta"));
                contrato.setCliente(rs.getString("cliente"));
                contrato.setId(rs.getInt("id"));
                contrato.setTipo_contrato(rs.getString("tipo_contrato"));
                contrato.setValor(rs.getString("valor"));
                contrato.setTitulo(rs.getString("titulo"));
                contrato.setInicio(rs.getString("inicio"));
                contrato.setConclusao(rs.getString("conclusao"));
                contrato.setTermino(rs.getString("termino"));
                contrato.setStatus(rs.getString("status"));
                lista.add(contrato);
            }
        } catch (SQLException e) {
            e.getMessage();
        }
        return lista;
    }
    public ArrayList consultarTodosContratos() {
        try {
            String sql = "select * from contrato";
            ArrayList<Contrato> lista = new ArrayList<>();
            rs=st.executeQuery(sql);
            while (rs.next()){
                Contrato contrato=new Contrato();
                contrato.setProposta(rs.getString("proposta"));
                contrato.setCliente(rs.getString("cliente"));
                contrato.setId(rs.getInt("id"));
                contrato.setTipo_contrato(rs.getString("tipo_contrato"));
                contrato.setValor(rs.getString("valor"));
                contrato.setTitulo(rs.getString("titulo"));
                contrato.setInicio(rs.getString("inicio"));
                contrato.setConclusao(rs.getString("conclusao"));
                contrato.setTermino(rs.getString("termino"));
                contrato.setStatus(rs.getString("status"));
                                             
                lista.add(contrato);
            }
            return lista;
        } catch (SQLException ex) {
            return null;
        }
    }  
    public ArrayList consultarContratoNome(String contratobusca) {
        try {
            String sql = "select * from contrato where cliente LIKE '%"+contratobusca+"%' OR titulo LIKE '%"+contratobusca+"%' OR proposta LIKE '%"+contratobusca+"%' OR tipo_contrato LIKE '%"+contratobusca+"%'";
            ArrayList<Contrato> lista = new ArrayList<>();
            rs=st.executeQuery(sql);
           
            while (rs.next()){
               
                Contrato contrato = new Contrato();
               
                contrato.setProposta(rs.getString("proposta"));
                contrato.setCliente(rs.getString("cliente"));
                contrato.setId(rs.getInt("id"));
                contrato.setData_proposta(rs.getString("data_proposta"));
                contrato.setTipo_contrato(rs.getString("tipo_contrato"));
                contrato.setValor(rs.getString("valor"));
                contrato.setForma_pagamento(rs.getString("forma_pagamento"));
                contrato.setIndice_reajuste(rs.getString("indice_reajuste"));
                contrato.setMes_reajuste(rs.getString("mes_reajuste"));
                contrato.setPrazo(rs.getString("prazo"));
                contrato.setTitulo(rs.getString("titulo"));
                contrato.setValidade(rs.getString("validade"));
                contrato.setDetalhes(rs.getString("detalhes"));
                contrato.setInicio(rs.getString("inicio"));
                contrato.setConclusao(rs.getString("conclusao"));
                contrato.setTermino(rs.getString("termino"));
                contrato.setStatus(rs.getString("status"));
                                             
                lista.add(contrato);
            }
            return lista;
        } catch (SQLException ex) {
            return null;
        }
    }
     public ArrayList consultarContratoId(int id) {
        try {
            String sql = "select * from contrato where id='"+id+"'";
            ArrayList<Contrato> lista = new ArrayList<>();
            rs=st.executeQuery(sql);
            while (rs.next()){
                Contrato contrato = new Contrato();
                
                contrato.setProposta(rs.getString("proposta"));
                contrato.setCliente(rs.getString("cliente"));
                contrato.setId(rs.getInt("id"));
                contrato.setData_proposta(rs.getString("data_proposta"));
                contrato.setTipo_contrato(rs.getString("tipo_contrato"));
                contrato.setValor(rs.getString("valor"));
                contrato.setForma_pagamento(rs.getString("forma_pagamento"));
                contrato.setIndice_reajuste(rs.getString("indice_reajuste"));
                contrato.setMes_reajuste(rs.getString("mes_reajuste"));
                contrato.setPrazo(rs.getString("prazo"));
                contrato.setTitulo(rs.getString("titulo"));
                contrato.setValidade(rs.getString("validade"));
                contrato.setDetalhes(rs.getString("detalhes"));
                contrato.setInicio(rs.getString("inicio"));
                contrato.setConclusao(rs.getString("conclusao"));
                contrato.setTermino(rs.getString("termino"));
                contrato.setStatus(rs.getString("status"));
                                             
                lista.add(contrato);
            }
            return lista;
        } catch (SQLException ex) {
            return null;
        }
    }
    public int consultarTotalContrato() throws Exception  {
        try {
            String sql = "select * from contrato";
            rs=st.executeQuery(sql);
            int count = 0;
            while (rs.next()){
            count++;    
            }
            return count;
        } catch (Exception e) {
            
        }
        return 0;
    }
    
    public float consultarMes6Contrato() throws Exception  {
        try {
            String sql = "SELECT * FROM contrato WHERE DATE_FORMAT(data_proposta, '%Y-%m') = DATE_FORMAT(curdate(), '%Y-%m')";
          
            rs=st.executeQuery(sql);
            float count = 0;
            while (rs.next()){
                String numero = rs.getString("valor");
                numero = numero.replace(".","");
                numero = numero.replace(",",".");
               count = count + Float.parseFloat(numero); 
            }
            return count;
        } catch (Exception e) {
            
        }
        return 0;
    }
    
    public float consultarMesIContrato(int i) throws Exception  {
        try {
            String sql = "SELECT * FROM contrato WHERE DATE_FORMAT(data_proposta, '%Y-%m') = DATE_FORMAT(curdate()-INTERVAL "+i+" MONTH, '%Y-%m')";
          
            rs=st.executeQuery(sql);
            float count = 0;
            while (rs.next()){
                String numero = rs.getString("valor");
                numero = numero.replace(".","");
                numero = numero.replace(",",".");
               count = count + Float.parseFloat(numero); 
            }
            return count;
        } catch (Exception e) {
            
        }
        return 0;
    }
     
    public int cadastraContrato(Contrato contrato) {
        try {
            
              String comando = "INSERT INTO contrato (id, proposta, cliente, data_proposta, tipo_contrato, valor, forma_pagamento, indice_reajuste, mes_reajuste, prazo, titulo, validade, detalhes, inicio, conclusao, termino, status) VALUES"
                      + "(NULL, '"+contrato.getProposta()+"', '"+contrato.getCliente()+"', STR_TO_DATE('"+contrato.getData_proposta()+"', '%d/%m/%Y'), '"+contrato.getTipo_contrato()+"', '"+contrato.getValor()+"', '"+contrato.getForma_pagamento()+"', '"+contrato.getIndice_reajuste()+"', '"+contrato.getMes_reajuste()+"', '"+contrato.getPrazo()+"', '"+contrato.getTitulo()+"', '"+contrato.getValidade()+"', '"+contrato.getDetalhes()+"', STR_TO_DATE('"+contrato.getInicio()+"', '%d/%m/%Y'), STR_TO_DATE('"+contrato.getConclusao()+"', '%d/%m/%Y'), STR_TO_DATE('"+contrato.getTermino()+"', '%d/%m/%Y'), '"+contrato.getStatus()+"')" ; 
              st.executeUpdate(comando);
            return 1;
        } catch (SQLException ex) {
            /* quando o programador deseja saber sobre o erro digitar as duas linhas de código abaixo:
            int codErro=ex.getErrorCode();
            String mensagemErro=ex.getMessage();*/
            if (ex.getErrorCode() == 1062) {
                return 2;
            } else {
                return 0;
            }
        }
    }
    
    public int alteraContrato(Contrato contrato) {
        try {
            
              String comando = "UPDATE contrato SET proposta='"+contrato.getProposta()+"',cliente='"+contrato.getCliente()+"',data_proposta=STR_TO_DATE('"+contrato.getData_proposta()+"', '%d/%m/%Y'), tipo_contrato='"+contrato.getTipo_contrato()+"',valor='"+contrato.getValor()+"',forma_pagamento='"+contrato.getForma_pagamento()+"',indice_reajuste='"+contrato.getIndice_reajuste()+"',mes_reajuste='"+contrato.getMes_reajuste()+"',prazo='"+contrato.getPrazo()+"',titulo='"+contrato.getTitulo()+"',validade='"+contrato.getValidade()+"',detalhes='"+contrato.getDetalhes()+"',inicio=STR_TO_DATE('"+contrato.getInicio()+"', '%d/%m/%Y'), conclusao=STR_TO_DATE('"+contrato.getConclusao()+"', '%d/%m/%Y'),termino=STR_TO_DATE('"+contrato.getTermino()+"', '%d/%m/%Y'),status='"+contrato.getStatus()+"'  WHERE id='"+contrato.getId()+"'" ; 
              st.executeUpdate(comando);
            return 1;
        } catch (SQLException ex) {
            /* quando o programador deseja saber sobre o erro digitar as duas linhas de código abaixo:
            int codErro=ex.getErrorCode();
            String mensagemErro=ex.getMessage();*/
             
            if (ex.getErrorCode() == 1062) {
                return 2;
            } else {
                return 0;
            }
        }
    
    }
    
    public void deletarContrato(int id){
    
    
          try {
                          
              String comando = "DELETE FROM contrato WHERE id='"+id+"'" ; 
              st.executeUpdate(comando);
           
        } catch (SQLException ex) {
            /* quando o programador deseja saber sobre o erro digitar as duas linhas de código abaixo:
            int codErro=ex.getErrorCode();
            String mensagemErro=ex.getMessage();*/
            
        }
    
    } 
    
}
