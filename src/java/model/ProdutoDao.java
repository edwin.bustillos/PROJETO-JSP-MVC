package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ProdutoDao {

    private final String user = "root";
    private final String password = "senha";
    private final String url = "jdbc:mysql://localhost:3306/dados";
    private Connection con;
    private Statement st;
    private ResultSet rs;

    public int conectar(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, user, password);
            st = con.createStatement();
            return 1;
        } catch (SQLException e) {
            e.getMessage();
            return 0;
        } catch (ClassNotFoundException e){
            e.getMessage();
            return 0;
        }
    }

    public int desconectar() {
        try {
            st.close();
            con.close();
            return 0;
        } catch (SQLException ex) {
           ex.getMessage();
           return 1;
        }
    }

    public List listar() {
        List<Produto> lista = new ArrayList<>();
        try {
            rs = st.executeQuery("SELECT * FROM produto");
            while (rs.next()) {
                Produto produto=new Produto();
                produto.setNome(rs.getString("nome"));
                produto.setCodigo(rs.getInt("codigo"));
                produto.setPreco(rs.getFloat("preco"));
                lista.add(produto);
            }
        } catch (SQLException e) {
            e.getMessage();
        }
        return lista;
    }
    
    public int alteraProduto(Produto pro) {
        try {
            
              String comando = "UPDATE produto SET nome='"+pro.getNome()+"',preco='"+(Float)pro.getPreco()+"' WHERE codigo='"+pro.getCodigo()+"'" ; 
              st.executeUpdate(comando);
            return 1;
        } catch (SQLException ex) {
            /* quando o programador deseja saber sobre o erro digitar as duas linhas de código abaixo:
            int codErro=ex.getErrorCode();
            String mensagemErro=ex.getMessage();*/
             
            if (ex.getErrorCode() == 1062) {
                return 2;
            } else {
                return 0;
            }
        }
    
    }
    
    public ArrayList consultarTodosProduto() {
        try {
            String sql = "select * from produto";
            ArrayList<Produto> lista = new ArrayList<>();
            rs=st.executeQuery(sql);
            while (rs.next()){
                Produto pro = new Produto();
                pro.setCodigo(rs.getInt("codigo"));
                pro.setNome(rs.getString("nome"));
                pro.setPreco(rs.getFloat("preco"));
                                             
                lista.add(pro);
            }
            return lista;
        } catch (SQLException ex) {
            return null;
        }
    }   
    
    public int consultarTotalProduto() throws Exception  {
        try {
            String sql = "select * from produto";
            rs=st.executeQuery(sql);
            int count = 0;
            while (rs.next()){
            count++;    
            }
            return count;
        } catch (Exception e) {
            
        }
        return 0;
    }
    
    public void deletarProduto(int id){
        
          try {
            
              String comando = "DELETE FROM produto WHERE codigo='"+id+"'" ; 
              st.executeUpdate(comando);
           
        } catch (SQLException ex) {
            /* quando o programador deseja saber sobre o erro digitar as duas linhas de código abaixo:
            int codErro=ex.getErrorCode();
            String mensagemErro=ex.getMessage();*/
            
        }
    
    }  
         public ArrayList consultarProdutoId(int id) {
        try {
            String sql = "select * from produto where codigo='"+id+"'";
            ArrayList<Produto> lista = new ArrayList<>();
            rs=st.executeQuery(sql);
            while (rs.next()){
                Produto pro = new Produto();
                pro.setCodigo(rs.getInt("codigo"));
                pro.setNome(rs.getString("nome"));
                pro.setPreco(rs.getFloat("preco"));
                                             
                lista.add(pro);
            }
            return lista;
        } catch (SQLException ex) {
            return null;
        }
    }
         
    public ArrayList consultarProdutoNome(String produto) {
        try {
            String sql = "select * from produto where nome LIKE '%"+produto+"%' OR codigo LIKE '%"+produto+"%'";
            ArrayList<Produto> lista = new ArrayList<>();
            rs=st.executeQuery(sql);
           
            while (rs.next()){
               
                Produto pro = new Produto();
               
                pro.setCodigo(rs.getInt("codigo"));
                pro.setNome(rs.getString("nome"));
                pro.setPreco(rs.getFloat("preco"));
                                             
                lista.add(pro);
            }
            return lista;
        } catch (SQLException ex) {
            return null;
        }
    }
         
    public int cadastraProduto(Produto pro) {
        try {
            
              String comando = "INSERT INTO produto (codigo,nome,preco)VALUES ('"+pro.getCodigo()+"','"+pro.getNome()+"','"+pro.getPreco()+"')" ; 
              st.executeUpdate(comando);
            return 1;
        } catch (SQLException ex) {
            /* quando o programador deseja saber sobre o erro digitar as duas linhas de código abaixo:
            int codErro=ex.getErrorCode();
            String mensagemErro=ex.getMessage();*/
            if (ex.getErrorCode() == 1062) {
                return 2;
            } else {
                return 0;
            }
        }
    }
}




 

