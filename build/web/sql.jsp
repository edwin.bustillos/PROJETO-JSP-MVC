<%@include file="header.jsp" %>
    <body>
         <div class="container-fluid" style="padding-top: 15px;">
    <div class="row">
        <div class="col-md-12 col-md-offset-6">
       <p class="h2">SQL DATA </p>
        <pre class="code">
<code>CREATE TABLE `cliente` (
  `id` int(10) NOT NULL,
  `nome` varchar(200) DEFAULT NULL,
  `telefone` varchar(30) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `idade` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `produto` (
  `codigo` int(10) NOT NULL,
  `nome` varchar(50) DEFAULT NULL,
  `preco` float(6,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `contrato` (
  `id` int(10) NOT NULL,
  `proposta` varchar(30) DEFAULT NULL,
  `cliente` varchar(40) DEFAULT NULL,
  `data_proposta` date DEFAULT NULL,
  `tipo_contrato` varchar(30) DEFAULT NULL,
  `valor` varchar(20) DEFAULT NULL,
  `forma_pagamento` varchar(30) DEFAULT NULL,
  `indice_reajuste` varchar(10) DEFAULT NULL,
  `mes_reajuste` varchar(20) DEFAULT NULL,
  `prazo` varchar(10) DEFAULT NULL,
  `titulo` varchar(50) DEFAULT NULL,
  `validade` varchar(20) DEFAULT NULL,
  `detalhes` text,
  `inicio` date DEFAULT NULL,
  `conclusao` date DEFAULT NULL,
  `termino` date DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

</code>

</pre>
        
         <a class="btn btn-secondary" href="index.jsp">Voltar a Tela Principal</a>
        </div></div></div>  </body>
</html>
