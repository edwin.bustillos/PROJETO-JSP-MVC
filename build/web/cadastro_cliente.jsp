<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>

<script type="text/javascript">
    $(function() {
  
  $('#tel').mask('(00)00000-0000',{placeholder:"(11)99999-9999"});
  

  });
</script>
<div class="container-fluid" style="padding-top: 15px;">
        
        <p class="h2">Cadastro de Clientes</p>
  
        <form  method="post" action="Controle" accept-charset="utf-8">
            <div class="form-row">
                <div class="form-group col-md-6">
                <input type="hidden" name="flag" value="cadastracli"/>
                <label for="nome">Nome: </label>
                <input type="text" class="form-control" id="nome" name="nome" size="30" placeholder="Nome" required="">
                </div>
                <div class="form-group col-md-6">
                    <label for="tel">Telefone:</label>
                    <input type="text" class="form-control" id="tel" name="tel" size="30" required="">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="idade">Idade:</label> 
                    <input type="number" class="form-control" id="idade" name="idade" size="8" placeholder="Idade" required="">
                </div>
                <div class="form-group col-md-6">
                    <label for="email">E-mail:</label>
                    <input type="email" class="form-control" id="email" name="email" size="40" placeholder="E-Mail" required="">
                </div>
            </div>
                
            <p><input type="submit" class="btn btn-primary" value="Enviar"> <input type="reset" class="btn btn-danger" value="Limpar">  </p>
        
        </form>
</div>
       
    </body>
</html>
