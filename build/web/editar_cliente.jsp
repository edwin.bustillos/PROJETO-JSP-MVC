<%@page import="model.Cliente"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
<script type="text/javascript">
    $(function() {
  
  $('#tel').mask('(00)00000-0000',{placeholder:"(11)99999-9999"});
  

  });
</script>
         <%
            ArrayList lista = (ArrayList) request.getAttribute("lista");
        %>
        <div class="container-fluid" style="padding-top: 15px;">
        
        <p class="h2">Edita Cliente</p>
          
       
        <form  method="get" action="Controle">
           
            <%
                 Cliente cliente = (Cliente) lista.get(0);
                    %>
            
             <div class="form-row">
                <div class="form-group col-md-6">
                    <input type="hidden" name="flag" value="salvacli"/>
             <input type="hidden" name="id" value="<%=cliente.getId()%>"/>
                    <label for="nome">Nome:</label>
                    <input class="form-control" type="text" id="nome" name="nome" value="<%=cliente.getNome()%>" size="30"> 
                </div>
                <div class="form-group col-md-6">
                    <label for="tel">Telefone:</label>
                    <input class="form-control" type="text" id="tel" name="tel" value="<%=cliente.getTel()%>" size="30">
                </div>
             </div>
             <div class="form-row">
                 <div class="form-group col-md-6">
                    <label for="idade">Idade:</label>
                    <input class="form-control" type="number" id="idade" name="idade" value="<%=cliente.getIdade()%>" size="8">
                 </div>
                 <div class="form-group col-md-6">
                     <label for="email">E-mail:</label>
                     <input class="form-control" type="email" id="email" name="email" value="<%=cliente.getEmail()%>" size="15"> 
                 </div>
                 
             </div>
            
                 <p> <input  class="btn btn-primary" type="submit" value="Alterar e Salvar">&nbsp;<a class="btn btn-danger"  href="Controle?flag=consultartodosclientes">Cancelar</a> </p>
                 
            
        </form>
        </div>
            </body>
</html>
