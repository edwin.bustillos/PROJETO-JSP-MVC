<%@page import="model.Contrato"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="header.jsp" %>
     <%
            ArrayList lista = (ArrayList) request.getAttribute("lista");
        %>
<div class="container-fluid" style="padding-top: 15px;">
        
        <p class="h2">Editar Contrato</p>
        
        <form action="Contratos" method="post">
            <%
                 Contrato contrato = (Contrato) lista.get(0);
                    %>
            <input type="hidden" name="id" value="<%=contrato.getId()%>">
                    <input type="hidden" name="flag" value="salvaContrato">
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="proposta">Proposta: </label>
                    <input type="text" class="form-control" id="proposta" name="proposta" value="<%=contrato.getProposta()%>"  placeholder="Codigo Proposta">
                </div>
                <div class="form-group col-md-8">
                   <label for="cliente">Contratado/Cliente: </label>
                   <input type="text" class="form-control" id="cliente" name="cliente" value="<%=contrato.getCliente()%>" placeholder="Nome / Razão Social">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-3" >
                    <label for="data_proposta">Data Proposta: </label>
                    <div class="input-group" >
                        <% String dt_proposta = contrato.getData_proposta();
                            String[] a = dt_proposta.split("-");
                            dt_proposta = a[2]+"/"+a[1]+"/"+a[0];
                         %>
                        <input type="text" class="form-control border-right-0" data-provide="datepicker" id ="data_proposta" name="data_proposta" value="<%=dt_proposta%>" required="" >
   <div class="input-group-prepend"><i class="input-group-text fas fa-calendar"></i></div>
                    </div>
                </div>
                
                <div class="form-group col-md-3">
                    <label for="tipo_contrato">Contrato: </label>
                    <input type="text" class="form-control" id="tipo_contrato" name="tipo_contrato" value="<%=contrato.getTipo_contrato()%>">
                </div>
                
                <div class="form-group col-md-3">
                <label for="valor">Valor R$: </label>
                    <div class="input-group" > 
                        <div class="input-group-prepend"><i class="input-group-text fas fa-money-check-alt"></i></div>
                        <input type="text" class="form-control" id="valor" name="valor" placeholder="00.000,00" value="<%=contrato.getValor()%>">
                </div>
                </div>
                <div class="form-group col-md-3">
                <label for="forma_pagamento">Forma Pagamento:</label>
                <select class="form-control" name="forma_pagamento" id="forma_pagamento">
                <option value="<%=contrato.getForma_pagamento()%>" selected><%=contrato.getForma_pagamento()%></option>
                <option value="Diario">Diario</option>
                <option value="Semanal">Semanal</option>
                <option value="Mensal">Mensal</option>
                <option value="Anual">Anual</option>
                </select>
                
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-3">
                <label for="indice_reajuste">Indice Reajuste:</label>
                <input class="form-control" type="number" step="0.01" id="indice_reajuste" name="indice_reajuste" value="<%=contrato.getIndice_reajuste()%>">
                </div>
                <div class="form-group col-md-3">
                <label for="mes_reajuste">Mês Reajuste:</label>
                <select class="form-control" name="mes_reajuste" id="mes_reajuste">
                <option value="<%=contrato.getMes_reajuste()%>" selected><%=contrato.getMes_reajuste()%></option>
                <option value="Janeiro">Janeiro</option>
                <option value="Fevereiro">Fevereiro</option>
                <option value="Marco">Marco</option>
                <option value="Abril">Abril</option>
                <option value="Maio">Maio</option>
                <option value="Junho">Junho</option>
                <option value="Julho">Julho</option>
                <option value="Agosto">Agosto</option>
                <option value="Setembro">Setembro</option>
                <option value="Outubro">Outubro</option>
                <option value="Novembro">Novembro</option>
                <option value="Dezembro">Dezembro</option>
                
                </select>
                </div>
                <div class="form-group col-md-3">
                <label for="prazo">Prazo:</label>
                <input type="text" class="form-control" id="prazo" name="prazo" placeholder="Prazo" value="<%=contrato.getPrazo()%>">
                </div>
                <div class="form-group col-md-3">
                <label for="validade">Validade:</label>
                <input type="text" class="form-control" id="validade" name="validade" placeholder="Validade" value="<%=contrato.getValidade()%>">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-3">
                <label for="inicio">Data Inicio:</label>   
                <div class="input-group" >
                    <% String dt_inicio = contrato.getInicio();
                             a = dt_inicio.split("-");
                            dt_inicio = a[2]+"/"+a[1]+"/"+a[0];
                         %>
                <input type="text" class="form-control" data-provide="datepicker" id="inicio" name="inicio" required="" value="<%=dt_inicio%>">
                <div class="input-group-prepend"><i class="input-group-text fas fa-calendar"></i></div>
                </div>
                </div>
                <div class="form-group col-md-3">
                <label for="conclusao">Data Conclusão:</label>
                <div class="input-group" >
                    <% String dt_conclusao = contrato.getConclusao();
                             a = dt_conclusao.split("-");
                            dt_conclusao = a[2]+"/"+a[1]+"/"+a[0];
                         %>
                <input type="text" class="form-control" data-provide="datepicker" id="conclusao" name="conclusao" required="" value="<%=dt_conclusao%>">
                <div class="input-group-prepend"><i class="input-group-text fas fa-calendar"></i></div>
                </div>
                </div>
                <div class="form-group col-md-3">
                <label for="termino">Data Termino:</label>    
                <div class="input-group" >
                    <% String dt_termino = contrato.getTermino();
                             a = dt_termino.split("-");
                            dt_termino = a[2]+"/"+a[1]+"/"+a[0];
                         %>
                <input type="text" class="form-control" data-provide="datepicker" id="termino" name="termino" required="" value="<%=dt_termino%>">
                <div class="input-group-prepend"><i class="input-group-text fas fa-calendar"></i></div>
                </div>
                </div>
                <div class="form-group col-md-3">
                <label for="status">Status:</label>   
                <select class="form-control" name="status" id="status">
                <option value="<%=contrato.getStatus()%>" selected><%=contrato.getStatus()%></option>
                <option value="Ativo">Ativo</option>
                <option value="Inativo">Inativo</option>
                </select>
                
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12"> 
                Titulo: <input type="text" class="form-control" name="titulo" size="30" placeholder="Titulo do Contrato" value="<%=contrato.getTitulo()%>">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12"> 
                    Detalhes:  <textarea name="detalhes" id="detalhes" ><%=contrato.getDetalhes()%></textarea>
                </div>
            </div>
                
                
                <p> <input type="submit" class="btn btn-primary" value="Enviar"> <input type="reset" class="btn btn-danger" value="Limpar">  
        
        </form>
   
 <script>
      $(document).ready(function() {
  $('#detalhes').summernote({
      height: 300,
        lang: 'pt-BR'
  });

$('#valor').mask("#.##0,00", {reverse: true});

$.fn.datepicker.dates['pt-BR']={days:["Domingo","Segunda","Terça","Quarta","Quinta","Sexta","Sábado"],daysShort:["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],daysMin:["Do","Se","Te","Qu","Qu","Se","Sa"],months:["Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],monthsShort:["Jan","Fev","Mar","Abr","Mai","Jun","Jul","Ago","Set","Out","Nov","Dez"],today:"Hoje",monthsTitle:"Meses",clear:"Limpar",format:"dd/mm/yyyy"};
$('#data_proposta,#inicio,#termino,#conclusao').datepicker({
     language: 'pt-BR'
});

});


    </script>
    </body>
</html>
