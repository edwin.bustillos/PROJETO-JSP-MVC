
<%@page import="model.Produto"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="header.jsp" %>
           
     
        <%
            ArrayList lista = (ArrayList) request.getAttribute("lista");
        %>
        <div class="container-fluid" style="padding-top: 15px;">
        
        <p class="h2">Lista de Produtos</p>

        <table id="lista-table" class="table table-bordered table-striped">
              <thead class="thead-info">
                  <tr>
                <th  width="10%">Id: </th>
                <th>Código: </th>
                <th>Nome: </th>
                <th>Preço: </th>      
                <th width="15%">Ação</th>
                  </tr>
            </thead>
            <tbody>
            <%
                for (int indice = 0; indice < lista.size(); indice++) {
                    Produto produto = (Produto) lista.get(indice);
            %>
            <tr >
                <td><%=indice+1%></td>
                <td><%=produto.getCodigo()%></td>
                <td><%=produto.getNome()%></td>
                <td><%=produto.getPreco()%></td>
                <td><a class="btn btn-info btn-sm" href="Controle?flag=alteraProduto&id=<%=produto.getCodigo()%>">Editar</a>&nbsp;<a class="btn btn-danger btn-sm" href="Controle?flag=deletaProduto&id=<%=produto.getCodigo()%>">Apagar</a></td>
            </tr>
            <%
                }
            %>
            </tbody>
        </table>
        </div>
 <script type="text/javascript">
  $('#lista-table').dynatable({
   
    features: {
    paginate: true,
    search: false,
    recordCount: true,
    perPageSelect: false
  },inputs: { 
    paginationLinkPlacement: 'after',
    paginationPrev: 'Anterior',
    paginationNext: 'Proximo',
   
    paginationGap: [1,2,2,1],
    searchTarget: 'por',
    searchPlacement: 'before',
    perPageTarget: null,
    perPagePlacement: 'before',
    perPageText: 'Mostrar: ',
    recordCountText: 'Mostrando: ',
    recordRecords:'Registros',
    processingText: 'Processando...'
  },
    params: {
      dynatable: 'dynatable',
      queries: 'queries',
      sorts: 'sorts',
      page: 'page',
      perPage: 'perPage',
      offset: 'offset',
      records: 'Registros',
      record: null,
      queryRecordCount: 'queryRecordCount',
      totalRecordCount: 'totalRecordCount'
    },
    dataset: {
      ajax: false,
      ajaxUrl: null,
      ajaxCache: null,
      ajaxOnLoad: false,
      ajaxMethod: 'GET',
      ajaxDataType: 'json',
      totalRecordCount: null,
      queries: {},
      queryRecordCount: null,
      page: null,
      perPageDefault: 10,
      perPageOptions: [10,20,50,100],
      sorts: {},
      sortsKeys: null,
      sortTypes: {},
      records: null
    }
    
    
  });</script>
    </body>
</html>
