<%@include file="header.jsp" %>
<div class="clearfix" style="padding-top: 10px;"></div>
<div class="container-fluid">
  <div class="row">
      <div class="col-md-6">
      
          <p class="h5"><b>Quantidades C/P/C</b></p>
          <canvas id="myChart" class="chartjs celula"></canvas>
<script>
new Chart(document.getElementById("myChart"),{
    "type":"bar",
    "data":{
        "labels":["Cliente","Produtos","Contratos"],
        "datasets":[{
                "label":"Totais",
                "data":[<%=request.getAttribute("clientes")%>,<%=request.getAttribute("produtos")%>,<%=request.getAttribute("contratos")%>],
                "fill":false,
                "backgroundColor":["rgba(54, 162, 235, 0.2)","rgba(153, 102, 255, 0.2)","rgba(201, 203, 207, 0.2)"],
                "borderColor":["rgb(54, 162, 235)","rgb(153, 102, 255)","rgb(201, 203, 207)"],
                "borderWidth":1}]},
    "options":{
        "scales":{
            "yAxes":[{
                    "ticks":{
                        "beginAtZero":true,
                    }
                }
            ]}
    }
});

</script>
    </div>
    <div class="col-md-6">
        <div>  
            <p class="h5"><b>Valor total em Contratos</b></p>
            <canvas id="chartjs-0" class="chartjs celula" width="undefined" height="undefined"></canvas>
            
            <script>new Chart(document.getElementById("chartjs-0"),{
                "type":"line",
                "data":{
                    "labels":["<%=request.getAttribute("mes1")%>","<%=request.getAttribute("mes2")%>","<%=request.getAttribute("mes3")%>","<%=request.getAttribute("mes4")%>","<%=request.getAttribute("mes5")%>","<%=request.getAttribute("mes6")%>"],
                    "datasets":[{
                            "label":"Semestral",
                            "data":[<%=request.getAttribute("contrato1")%>,<%=request.getAttribute("contrato2")%>,<%=request.getAttribute("contrato3")%>,<%=request.getAttribute("contrato4")%>,<%=request.getAttribute("contrato5")%>,<%=request.getAttribute("contrato6")%>],
                            "fill":false,
                            "borderColor":"rgb(75, 192, 192)","lineTension":0.1
                        }]
                },
                        "options":{
                            animation: {
            duration: 1000, // general animation time
        },
        hover: {
            animationDuration:1000, // duration of animations when hovering an item
        },
        responsiveAnimationDuration: 1000, // animation duration after a resize
                        }});</script></div>
    </div>
    

</div>
<div class="clearfix" style="padding-top: 10px;"></div>
<div class="container-fluid">
<div style="align-content: center;"><h2>Seja bem vindo...
</h2></div>

    <div class="row">
        <div class="col-md-12 col-md-offset-6">
          
    <p>    PROJETO CRIADO PARA A MAT�RIA PROGRAMA��O ORIENTADA A OBJETOS</p>
    <p>
        CURSO: CI�NCIA DA COMPUTA��O (CAMPUS BARRA FUNDA / MANH�/ SALA 301)</p>
    <p>
        UNIVERSIDADE NOVE DE JULHO</p>
    <p>
        PROF. GERSON RISSO</p>
    <p>
INTEGRANTES:
    </p><p>
EMMANUEL, EDWIN e WELINGTON
    </p>

    <a class="btn btn-secondary" href="sql.jsp">Ver SQL Tabela</a>
    <br><br>  </div></div>
       
    </div>    
    </body>
</html>
