<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <title>Projeto Sistema MVC</title>
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/jquery.dynatable.css">
         <link href="js/summer/summernote-lite.css" rel="stylesheet">
          <link href="css/datepicker.css" rel="stylesheet">
          <link href="css/style.css" rel="stylesheet">
        <script src="js/jquery-slim.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/jquery.dynatable.js"></script>
<script src="js/summer/summernote-lite.js"></script>
<script src="js/summer/lang/summernote-pt-BR.js"></script>
<script src="js/jquery.mask.js"></script>
<script src="js/chart.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous"> 

    </head>
    <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light" style="padding-bottom: 10px; border-bottom: solid 2px #cccccc;">
  <a class="navbar-brand" href="index.jsp">Sistema MVC</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Clientes
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="cadastro_cliente.jsp">Novo Cadastro</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="Controle?flag=consultartodosclientes">Listar Clientes</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Produtos
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="cadastro_produto.jsp">Novo Cadastro</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="Controle?flag=consultartodosproduto">Listar Produtos</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Contratos
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="cadastro_contrato.jsp">Novo Cadastro</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="Contratos?flag=consultatodoscontratos">Listar Contratos</a>
        </div>
      </li>
      
    </ul>
    <form class="form-inline my-2 my-lg-0" method="get" action="Busca">
      Buscar em: &nbsp;
        <select class="form-control" id="flag" name="flag">
            <option value="clientes">Clientes</option>
            <option value="produtos">Produtos</option>
            <option value="contratos">Contratos</option>
    </select>&nbsp;
      <input class="form-control mr-sm-2" type="search" placeholder="Digite aqui" aria-label="busca" name="busca">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Procurar</button>
    </form>
  </div>
</nav>
        