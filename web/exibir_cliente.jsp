
<%@page import="model.Cliente"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="header.jsp" %>
           
     
        <%
            ArrayList lista = (ArrayList) request.getAttribute("lista");
        %>
        <div class="container-fluid" style="padding-top: 15px;">
        
        <p class="h2">Lista de Clientes</p>
        <table id="lista-table" class="table table-bordered table-striped">
              <thead class="thead-info">
                  <tr>
                <th>Id: </th>
                <th>Nome: </th>
                <th>Telefone: </th>
                <th>Email: </th>
                <th>Idade: </th>        
                <th width="20%">Ação</th>
                  </tr>
            </thead>
            <tbody>
            <%
                for (int indice = 0; indice < lista.size(); indice++) {
                    Cliente cliente = (Cliente) lista.get(indice);
            %>
            <tr>
                <td scope="row"><%=indice+1%></td>
                <td><%=cliente.getNome()%></td>
                <td><%=cliente.getTel()%></td>
                <td><%=cliente.getEmail()%></td>
                <td><%=cliente.getIdade()%></td> 
                <td><a class="btn btn-info btn-sm" href="Controle?flag=alteraCliente&id=<%=cliente.getId()%>">Editar</a>&nbsp;<a class="btn btn-danger btn-sm" href="Controle?flag=deletaCliente&id=<%=cliente.getId()%>">Apagar</a></td>
            </tr>
            <%
                }
            %>
            </tbody>
        </table>
    </div>
    <script type="text/javascript">
  $('#lista-table').dynatable({
   
    features: {
    paginate: true,
    search: false,
    recordCount: true,
    perPageSelect: false
  },inputs: { 
    paginationLinkPlacement: 'after',
    paginationPrev: 'Anterior',
    paginationNext: 'Proximo',
   
    paginationGap: [1,2,2,1],
    searchTarget: 'por',
    searchPlacement: 'before',
    perPageTarget: null,
    perPagePlacement: 'before',
    perPageText: 'Mostrar: ',
    recordCountText: 'Mostrando: ',
    recordRecords:'Registros',
    processingText: 'Processando...'
  },
    params: {
      dynatable: 'dynatable',
      queries: 'queries',
      sorts: 'sorts',
      page: 'page',
      perPage: 'perPage',
      offset: 'offset',
      records: 'Registros',
      record: null,
      queryRecordCount: 'queryRecordCount',
      totalRecordCount: 'totalRecordCount'
    },
    dataset: {
      ajax: false,
      ajaxUrl: null,
      ajaxCache: null,
      ajaxOnLoad: false,
      ajaxMethod: 'GET',
      ajaxDataType: 'json',
      totalRecordCount: null,
      queries: {},
      queryRecordCount: null,
      page: null,
      perPageDefault: 10,
      perPageOptions: [10,20,50,100],
      sorts: {},
      sortsKeys: null,
      sortTypes: {},
      records: null
    }
    
    
  });</script>
    </body>
</html>
